/* eslint-disable class-methods-use-this */
'use strict';
const Project = use('App/Model/Project');
const Database = use('Database');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with projects
 */
class ProjectController {
  /**
   * Show a list of all projects.
   * GET projects
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({  response }) {
    const projects = await Project.all();
    response.status(200).json({
      success: true,
      data: projects,
    });
  }

  /**
   * Create/save a new project.
   * POST projects
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { name, description, customer_id } = request.post();
    const project = await Project.create({ name, description, customer_id });
    response.status(201).json({
      success: true,
      data: project,
    });
  }

  /**
   * Display a single project.
   * GET projects/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request, response }) {
    response.status(200).json({
      success: true,
      data: request.post().project,
    });
  }


  /**
   * Update project details.
   * PUT or PATCH projects/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request, response }) {
    const {
      name,
      description,
      customer_id,
      project,
    } = request.post();

    project.name = name;
    project.description = description;
    project.customer_id = customer_id;

    await project.save();

    response.status(200).json({
      success: true,
      data: project,
    });
}

  /**
   * Delete a project with id.
   * DELETE projects/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params:{ id }, request, response }) {
    const { project } = request.post();
    await project.delete();
    response.status(200).json({
      success: true,
      message: 'Successfully deleted this project.',
      id,
    });
  }
}

module.exports = ProjectController;
