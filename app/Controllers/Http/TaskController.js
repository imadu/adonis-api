/* eslint-disable class-methods-use-this */


const Task = use('App/Models/Task');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with tasks
 */
class TaskController {
  /**
   * Show a list of all tasks.
   * GET tasks
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response }) {
    const tasks = await Task.all();
    response.status(200).json({
      success: true,
      message: 'Here are your tasks',
      data: tasks,
    });
  }


  /**
   * Create/save a new task.
   * POST tasks
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { name, description, project_id } = request.post();

    const task = await Task.create({ name, description, project_id });

    response.status(201).json({
      success: true,
      message: 'Successfully created a new task.',
      data: task,
    });
  }

  /**
   * Display a single task.
   * GET tasks/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request, response }) {
    response.status(200).json({
      message: 'Here is your task.',
      data: request.post().task,
    });
  }

  /**
   * Update task details.
   * PUT or PATCH tasks/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params: { id }, request, response }) {
    const {
      name,
      description,
      // eslint-disable-next-line camelcase
      project_id, task,
    } = request.post();
    task.name = name;
    task.description = description;
    // eslint-disable-next-line camelcase
    task.project_id = project_id;

    await task.save();
    response.status(200).json({
      success: true,
      message: 'Sucessfully updated the data',
      data: task,
    });
  }

  /**
   * Delete a task with id.
   * DELETE tasks/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, request, response }) {
    const { task } = request.post();
    await task.delete();
    response.status(200).json({
      message: 'Deleted task.',
      id,
    });
  }
}

module.exports = TaskController;
